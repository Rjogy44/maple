//
//  Name+CoreDataProperties.h
//  MapApp
//
//  Created by Randy Jorgensen on 3/17/16.
//  Copyright © 2016 Randy Jorgensen. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Name.h"

NS_ASSUME_NONNULL_BEGIN

@interface Name (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSOrderedSet<NSManagedObject *> *address;

@end

@interface Name (CoreDataGeneratedAccessors)

- (void)insertObject:(NSManagedObject *)value inAddressAtIndex:(NSUInteger)idx;
- (void)removeObjectFromAddressAtIndex:(NSUInteger)idx;
- (void)insertAddress:(NSArray<NSManagedObject *> *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeAddressAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInAddressAtIndex:(NSUInteger)idx withObject:(NSManagedObject *)value;
- (void)replaceAddressAtIndexes:(NSIndexSet *)indexes withAddress:(NSArray<NSManagedObject *> *)values;
- (void)addAddressObject:(NSManagedObject *)value;
- (void)removeAddressObject:(NSManagedObject *)value;
- (void)addAddress:(NSOrderedSet<NSManagedObject *> *)values;
- (void)removeAddress:(NSOrderedSet<NSManagedObject *> *)values;

@end

NS_ASSUME_NONNULL_END
