//
//  Address.h
//  MapApp
//
//  Created by Randy Jorgensen on 3/17/16.
//  Copyright © 2016 Randy Jorgensen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Name;

NS_ASSUME_NONNULL_BEGIN

@interface Address : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Address+CoreDataProperties.h"
