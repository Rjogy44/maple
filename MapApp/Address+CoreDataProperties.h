//
//  Address+CoreDataProperties.h
//  MapApp
//
//  Created by Randy Jorgensen on 3/17/16.
//  Copyright © 2016 Randy Jorgensen. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Address.h"

NS_ASSUME_NONNULL_BEGIN

@interface Address (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *address;
@property (nullable, nonatomic, retain) Name *relationship;

@end

NS_ASSUME_NONNULL_END
