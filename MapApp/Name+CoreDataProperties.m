//
//  Name+CoreDataProperties.m
//  MapApp
//
//  Created by Randy Jorgensen on 3/17/16.
//  Copyright © 2016 Randy Jorgensen. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Name+CoreDataProperties.h"

@implementation Name (CoreDataProperties)

@dynamic name;
@dynamic address;

@end
