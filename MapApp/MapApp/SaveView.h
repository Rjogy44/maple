//
//  SaveView.h
//  MapApp
//
//  Created by Randy Jorgensen on 3/15/16.
//  Copyright © 2016 Randy Jorgensen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "Name.h"
#import "Address.h"

@interface SaveView : UIViewController
{
    NSString* nameString;
    NSString* addressString;
    
    
    __weak IBOutlet UITextField *nameField;

    __weak IBOutlet UITextField *addressField;
}


@end

