//
//  SaveView.m
//  MapApp
//
//  Created by Randy Jorgensen on 3/15/16.
//  Copyright © 2016 Randy Jorgensen. All rights reserved.
//

#import "SaveView.h"
#import "AppDelegate.h"

@interface SaveView ()

@end

@implementation SaveView




- (void)viewDidLoad {
    [super viewDidLoad];
    

//    

    // Do any additional setup after loading the view, typically from a nib.
}

- (IBAction)saveName:(UITextField *)sender {
    

}
- (IBAction)saveAddress:(UITextField *)sender {
    
}


- (IBAction)Save:(UIButton *)sender {
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSManagedObject* name = [NSEntityDescription insertNewObjectForEntityForName:@"Name" inManagedObjectContext:context];
    NSManagedObject* address = [NSEntityDescription insertNewObjectForEntityForName:@"Address" inManagedObjectContext:context];
    [name setValue:nameField.text forKey:@"name"];
    [address setValue:addressField.text forKey:@"address"];
    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"Save Failed! %@ %@", error, [error localizedDescription]);
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
