//
//  AppDelegate.h
//  MapApp
//
//  Created by Randy Jorgensen on 3/15/16.
//  Copyright © 2016 Randy Jorgensen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic, readwrite) int nameMap;
@property (nonatomic, readwrite) NSString* addressMap;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;


@end

