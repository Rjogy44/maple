//
//  ViewController.h
//  MapApp
//
//  Created by Randy Jorgensen on 3/15/16.
//  Copyright © 2016 Randy Jorgensen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MapView.h"

@interface ViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    UITableViewCell *cell;
    NSArray* fetched;
    NSArray* fetched1;
 
    IBOutlet UITableView *cView;
}



@end

