//
//  MapVIew.m
//  MapApp
//
//  Created by Randy Jorgensen on 3/15/16.
//  Copyright © 2016 Randy Jorgensen. All rights reserved.
//

#import "MapView.h"
#import "AppDelegate.h"

@interface MapView ()

@end

@implementation MapView
    
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    
    NSError* error;
    
    NSFetchRequest* request = [[NSFetchRequest alloc]init];
    NSEntityDescription* entity = [NSEntityDescription entityForName:@"Name" inManagedObjectContext:context];
    [request setEntity:entity];
    fetched = [context executeFetchRequest:request error:&error];
    
    
    
    NSFetchRequest* request1 = [[NSFetchRequest alloc]init];
    NSEntityDescription* entity1 = [NSEntityDescription entityForName:@"Address" inManagedObjectContext:context];
    [request1 setEntity:entity1];
    fetched1 = [context executeFetchRequest:request1 error:&error];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSArray *values = [fetched valueForKeyPath: @"name"];
    NSArray *values1 = [fetched1 valueForKeyPath: @"address"];
    
    
    text.text = values[appDelegate.nameMap];
    NSString *points = values1[appDelegate.nameMap];
    
    locationManager = [[CLLocationManager alloc]init];
    locationManager.delegate = self;
    
    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [locationManager requestWhenInUseAuthorization];
    }
    [locationManager startUpdatingLocation];
    
    
        
    if(self.geocoder == nil)
            
        {
            
            self.geocoder = [[CLGeocoder alloc] init];
            
        }
        
    NSString *address = points;
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder geocodeAddressString:address
                 completionHandler:^(NSArray* placemarks, NSError* error){
                     if (placemarks && placemarks.count > 0) {
                         CLPlacemark *topResult = [placemarks objectAtIndex:0];
                         MKPlacemark *placemark = [[MKPlacemark alloc] initWithPlacemark:topResult];
                         
                         MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(placemark.coordinate, 800, 800);
                         
                         [self.mapView setRegion:region animated:YES];
                         [self.mapView addAnnotation:placemark];
                     }
                 }
     ];
    
//    CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(41.195064, -111.941835);
//    point.coordinate = coord;
//    point.title = @"Where am I?";
//    point.subtitle = @"I'm here!!!";
//    
//    [self.mapView addAnnotation:point];
    
    
    [super viewWillAppear:animated];
   
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end