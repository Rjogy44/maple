//
//  MapView.h
//  MapApp
//
//  Created by Randy Jorgensen on 3/15/16.
//  Copyright © 2016 Randy Jorgensen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface MapView : UIViewController <MKMapViewDelegate, CLLocationManagerDelegate>
{
    NSArray* fetched;
    IBOutlet UITextField *text;
    NSArray* fetched1;
    CLLocationManager* locationManager;

}
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) CLGeocoder *geocoder;

@end
