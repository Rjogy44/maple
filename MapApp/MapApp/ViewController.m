//
//  ViewController.m
//  MapApp
//
//  Created by Randy Jorgensen on 3/15/16.
//  Copyright © 2016 Randy Jorgensen. All rights reserved.
//

#import "ViewController.h"
#import "MapView.h"


@interface ViewController ()

@end

@implementation ViewController





- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    // Do any additional setup after loading the view, typically from a nib.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return fetched.count;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell = [cell initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
  
    NSArray *values = [fetched valueForKeyPath: @"name"];
    NSArray *values1 = [fetched1 valueForKeyPath: @"address"];
   
    
    cell.textLabel.text = values[indexPath.row];
    cell.detailTextLabel.text = values1[indexPath.row];
    
    
    
    return cell;

    
}
-(void)viewWillAppear:(BOOL)animated
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    
    NSError* error;
    
    NSFetchRequest* request = [[NSFetchRequest alloc]init];
    NSEntityDescription* entity = [NSEntityDescription entityForName:@"Name" inManagedObjectContext:context];
    [request setEntity:entity];
    fetched = [context executeFetchRequest:request error:&error];
    
    
    
    NSFetchRequest* request1 = [[NSFetchRequest alloc]init];
    NSEntityDescription* entity1 = [NSEntityDescription entityForName:@"Address" inManagedObjectContext:context];
    [request1 setEntity:entity1];
    fetched1 = [context executeFetchRequest:request1 error:&error];
    
    
    [cView reloadData];
    
    [super viewWillAppear:animated];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
     appDelegate.nameMap = (int)indexPath.row;
    
    
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
