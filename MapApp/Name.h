//
//  Name.h
//  MapApp
//
//  Created by Randy Jorgensen on 3/17/16.
//  Copyright © 2016 Randy Jorgensen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Name : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Name+CoreDataProperties.h"
